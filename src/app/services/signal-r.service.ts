import { Injectable } from '@angular/core';

import * as signalR from '@aspnet/signalr';
import { ChartModel } from '../models/chartModel.interface';


@Injectable({
  providedIn: 'root'
})
export class SignalRService {

  public data: ChartModel[];
  private hubConnection: signalR.HubConnection;

  public bradcastedData: ChartModel[];

  constructor() { }

  // ***************    Recevoir Data Depuis le Serveur   ***************
  public startConnection = () => {
    // nous construisons.
    this.hubConnection = new signalR.HubConnectionBuilder()
                            // Adress route SignalR
                            .withUrl('https://localhost:44346/chart')
                            .build();

    // démarrons notre connexion
    this.hubConnection.start()
                      .then(() => console.log('Connection started'))
                      .catch(err => console.log('Error while starting connection: ' + err));
  }

  public GetDataChartService = () => {
    // mm nom que la methode du hub Server
    this.hubConnection.on('SendDataChartServer', (data) => {
      this.data = data;
      console.log(data);
    });
  }


  // ***************    Envoi Data Depuis le Client    ***************
  // will send data to our Hub endpoint
  public SendDataByClient = () => {
    this.hubConnection.invoke('broadcastchartdata', this.data)
    .catch(err => console.error(err));
  }

  // will listen on the braodcastchartdata event.
  public GetDataFromServer = () => {
    this.hubConnection.on('broadcastchartdata', (data) => {
      this.bradcastedData = data;
    });
  }

}
